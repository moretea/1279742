namespace :ts do
  # Workaround to make Sphinx run in the foreground
  task :run_in_foreground => [ 'ts:conf', 'ts:in' ] do
    ts = ThinkingSphinx::Configuration.instance

    $kill_sphinx_on_exit = false
    unless pid = fork
      exec "#{ts.bin_path}#{ts.searchd_binary_name} --pidfile --config #{ts.config_file} --nodetach"
    end

    $kill_sphinx_on_exit = true

    at_exit do
      if $kill_sphinx_on_exit
        Process.kill(:TERM, pid)
      end
    end

    Process.wait
  end
end